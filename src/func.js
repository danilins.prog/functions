const getSum = (str1, str2) => {
  if (Array.isArray(str1) || Array.isArray(str2)) { return false; }
  if (isNaN(Number(str1)) && isNaN(Number(str2))) { return false;}
  for (let i = 0; i < str1.length; i++) {
    if (!Number.isInteger(Number(str1[i]))) {
      return false;
    }
  }
  for (let i = 0; i < str2.length; i++) {
    if (!Number.isInteger(Number(str2[i]))) {
      return false;
    }
  }
  if (str1.length !== str2.length) {
    if (str1.length > str2.length) {
      var abs = str1.length - str2.length;
      for (var i = 0; i < abs; i++) {
        str2 = '0' + str2;
      }
    } else {
      var abss = str2.length - str1.length;
      for (var k = 0; k < abss; k++) {
        str1 = '0' + str1;
      }
    }
  }
  var str = '';
  var currentNum = '';
  var nextPos;
  for (var j = str1.length - 1; j >= 0; j--) {
    if ((Number(str1[j])) + (Number(str2[j])) < 10) {
      str = Number(str1[j]) + Number(str2[j]) + str;
      nextPos = 0;
    } else {
      str = Number(str1[j]) + Number(str2[j]);
      str = String(str);

      currentNum = (str.slice(1, 2));
      str = currentNum;
      nextPos = 1 ;
    }
  }
  return String(Number(str1) + Number(str2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let PostCount = 0;
  let CommentsCount = 0;
  if (listOfPosts !== undefined) {
    for (let i = 0; i < listOfPosts.length; i++) {
      if (listOfPosts[i].author === String(authorName)) {
        PostCount++;
      }
      if (listOfPosts[i].comments !== undefined) {
        for (let j = 0; j < listOfPosts[i].comments.length; j++) {
          if (listOfPosts[i].comments[j].author === String(authorName)) {
            CommentsCount++;
          }
        }
      }
    }
  }
  return String("Post:") + PostCount + ",comments:" + CommentsCount;
};

const tickets = (people) => {
  let bank = 0;
  for (let i = 0; i < people.length; i++) {
    if (people[i] - 25 > bank) {
      return "NO";
    }
    else if (people[i] - 25 === 0) {
      bank += 25;
    }
    else if (bank !== people[i] - 25 && people[i] !== 50) {
      bank -= people[i] - 25;
    }
  }
  return "YES";
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
